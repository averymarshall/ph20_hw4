\documentclass[11pt]{article}

\usepackage[retainorgcmds]{IEEEtrantools}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{float}
\usepackage{fancyhdr}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage{lastpage}
\pagestyle{fancy}
\fancyhf{}

\rfoot{Page \thepage \hspace{1pt} of \pageref{LastPage}}
\begin{document}

\lhead{Ph20 - Spring 2015}
\chead{\textbf{\LARGE Homework 3}}
\rhead{Avery Marshall}

\section{Numerical solution to spring-mass system}
\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{part1}
    \caption{$x$ and $v$ as functions of time for $h = 0.001$}
    \label{fig:part1}
\end{figure}

\section{Analytical solution to spring-mass system}
With the given initial conditions that $\frac{k}{m} = 1$ and my chosen initial conditions that $x = 1$ and $v = 0$, the equation of motion for the spring-mass system becomes
\begin{gather}
a = -x
\\
\frac{d^2x}{dx^2} + x  = 0
\\
x = A\sin(t) + B\cos(t)
\\
x`= v = A\cos(t) - B\sin(t)
\end{gather}
Using initial conditions we can solve for A and B analytically.
\begin{gather}
x(0) = 1 = A\cdot 0 + B\cdot 1 = B
\\
x`(0) = 0 = A\cdot 1 - B\cdot 0 = A
\end{gather}
Hence the analytical solutions for the position and velocity of the spring-mass system are
\begin{gather}
x = \cos(t)
\\
v = -\sin(t)
\end{gather}
The errors between this analytical solution and the numerical solution exhibited in part 1 are shown below.
\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{part2}
    \caption{Errors in $x$ and $v$ as functions of time for $h = 0.001$}
    \label{fig:part2}
\end{figure}
It can be seen that the error starts off very small, but quickly increases. The maximum deviation of the error is increasing linearly with time, although it oscillates between positive and negative extrema.

\section{Truncation error}
The graph below exhibits the dependence of the maximum error with h.
\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{part3}
    \caption{Maximum error in $x(t)$ and $v(t)$ vs. step size $h$}
    \label{fig:part3}
\end{figure}

\section{Energy of the spring-mass system}
Analytically, the derivation of the energy of the spring-mass system is quite trivial:
\begin{IEEEeqnarray}{rCl}
E & = & x(t)^2 + v(t)^2
\\
& = & (\cos(t))^2 + (-\sin(t))^2
\\
& = & \cos^2(t) + \sin^2(t)
\\
& = & 1
\end{IEEEeqnarray}
Hence, it is clear that the energy should be constant. However, the graph of the numerical evolution of the energy yields a quite different result.
\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{part4}
    \caption{Numerical evolution of E for the explicit Euler method}
    \label{fig:part4}
\end{figure}
The long term trend of the energy is for the error to grow linearly from the correct value of 1, similar to the linear increase of the error in $x(t)$ and $v(t)$.

\section{Implicit Euler method}
First, one is tasked with solving for $x_{i+1}$ and $v_{i+1}$ by solving the following linear equation:
\begin{gather}
\begin{bmatrix}
1 & -h \\
h & 1 
\end{bmatrix}
\cdot
\begin{bmatrix}
x_{i+1} \\
v_{i+1}
\end{bmatrix}
=
\begin{bmatrix}
x_{i} \\
v_{i}
\end{bmatrix}
\end{gather}
Yielding the solutions
\begin{gather}
x_{i+1} = \frac{x_i + hv_i}{1 + h^2}
\\
v_{i+1} = \frac{v_i - hx_i}{1 + h^2}
\end{gather}
These solutions are only a factor of $\frac{1}{1 + h^2}$ away from their explicit counterparts.
\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{part5-1}
    \caption{Errors (implicit) in $x$ and $v$ as functions of time for $h = 0.001$}
    \label{fig:part5-1}
\end{figure}
Unsurprisingly, graphs of the global errors for $x$ and $v$ evaluated implicitly are very similar to the global errors evaluated explicitly. However, a closer examination will reveal that the amplitudes of the error functions are flipped by a factor of $-1$. A graph of the energy error (calculated implicitly) shall be even more telling. 
\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{part5-2}
    \caption{Numerical evolution of E for the implicit Euler method}
    \label{fig:part5-2}
\end{figure}
The slope is exactly the opposite of the slope exhibited for Figure 4. Hence, the implicit Euler method tends to give the same error as the explicit method, except reversed. It also exhibits a trend of linearly unbounded growth of error, except it is in the negative direction.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Part 2
\section{Phase space of implicit and explicit Euler methods}
Analytically, a graph of the phase space of $x(t)$ vs. $v(t)$ should be a perfect circle, since $x^2 + v^2 = E$ and energy is conserved. However, the explicit Euler method was shown in the previous section to lack conservation of energy as a consequence of its inherent error. This error is further demonstrated by the elliptical shape of the phase space graph shown below.
\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{part6-1}
    \caption{Phase space for explicit euler method with $h = 0.001$}
    \label{fig:part6-1}
\end{figure}
The implicit Euler method also fails conservation of energy and, hence, also exhibits this elliptical deviation from a perfect circle.
\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{part6-2}
    \caption{Phase space for implicit euler method with $h = 0.001$}
    \label{fig:part6-2}
\end{figure}

\section{Phase space of symplectic Euler method}
The symplectic Euler method also exhibits elliptical behavior, since it deviates from a perfect circle.
\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{part7-1}
    \caption{Phase space for symplectic euler method with $h = 0.001$}
    \label{fig:part7-1}
\end{figure}
\end{document}
