'''
Numerically solves the differential equation for a spring-mass system
using the symplectic Euler method.

Usage: symplecticEuler.py [filename]

Use p if a plot is desired.

filename: name of the file to save plot to.
'''
import matplotlib.pyplot as plt
import numpy as np
import sys

# Initial conditions for a spring stretched from equilibrium and at rest.
# h = .001 and N = 50000 work well for producing a fairly accurate plot.
x0 = 1
v0 = 0
h = .001
t_f = 100 # final time 
x = [x0]
v = [v0]
t = np.linspace(0, t_f, (int)(t_f/h))
# Generating data set for x and v
for i in range(1, (int)(t_f/h)):
    x.append(x[i-1] + h*v[i-1])
    v.append(v[i-1] - h*x[i])

# Converting to numpy arrays
x = np.array(x)
v = np.array(v)

# Saving
np.savetxt("xs.txt", x)
np.savetxt("vs.txt", v)
np.savetxt("t.txt", t)	

# Plotting if necessary
if len(sys.argv) is 2:
    fig, (ax1, ax2) = plt.subplots(1, 2)
    ax1.set_title("Position vs. Time")
    ax1.plot(t, x)

    ax2.set_title("Velocity vs. Time")
    ax2.plot(t, v)

    plt.draw()
    plt.savefig(sys.argv[1])
    
    
