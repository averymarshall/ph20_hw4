'''
Plots the maximum explicit error vs. different values of h. 

Usage: truncError.py [filename]

filename: name of the file to save plot to.
'''
import matplotlib.pyplot as plt
import numpy as np
import sys

filename = sys.argv[1]

def pos(x):
    ''' Defines the position function for the system. Inputs and 
    outputs are numpy arrays.'''
    return np.cos(x)

def vel(x):
    '''Defines the velocity function for the system. Inputs and outputs
    are numpy arrays.'''
    return -np.sin(x)

# Initial conditions for a spring stretched from equilibrium and at rest.
# h = .001 and N = 50000 work well for producing a fairly accurate plot.
x0 = 1
v0 = 0
h0 = .001
N = 50000 # Number of steps

# Declaring maximum arrays
xMax = []
vMax = []

h_list = [h0/(2**x) for x in range(0, 5)]

# Generating maximum error data set
for h in h_list:
    t = np.linspace(0, N*h, N+1)
    xNum = [x0]
    vNum = [v0]
    
    # Generating data set for x and v
    for i in range(1,N+1):
        xNum.append(xNum[i-1] + h*vNum[i-1])
        vNum.append(vNum[i-1] - h*xNum[i-1])

    # Converting to numpy arrays
    xNum = np.array(xNum)
    vNum = np.array(vNum)
    
    # Generating analytical data
    xAnal = pos(t)
    vAnal = vel(t) 

    # Generating the error between the two
    xErr = xAnal - xNum
    vErr = vAnal - vNum

    # Saving the maximum error
    xMax.append(xErr.max())
    vMax.append(vErr.max())

# Converting to numpy arrays
h_list = np.array(h_list)
xMax = np.array(xMax)
vMax = np.array(vMax)

fig, (ax1, ax2) = plt.subplots(1, 2)
ax1.set_title("Position Max Error vs. Step")
ax1.plot(h_list, xMax)

ax2.set_title("Velocity Max Error vs. Step")
ax2.plot(h_list, vMax)

plt.draw()
plt.savefig(filename)
