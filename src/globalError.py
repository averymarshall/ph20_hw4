'''
Plots the difference between the analytical and numerical solutions to
a spring mass system. Gets data for the numerical solution from 'x.txt',
'v.txt', and 't.txt'. explicitEuler.py should be ran first in the same
source directory before running this script.

Usage: globalError.py [option] [filename]

option: 'e' for explicit Euler data, 'i' for implicit Euler data.
filename: name of the file to save plot to.
'''

import matplotlib.pyplot as plt
import numpy as np
import sys

filename = sys.argv[2]

def pos(x):
    ''' Defines the position function for the system. Inputs and 
    outputs are numpy arrays.'''
    return np.cos(x)

def vel(x):
    '''Defines the velocity function for the system. Inputs and outputs
    are numpy arrays.'''
    return -np.sin(x)
    
# Getting numerical solution data from .txt files.
if sys.argv[1] is 'e':
    xNum = np.loadtxt("x.txt")
    vNum = np.loadtxt("v.txt")
    t = np.loadtxt("t.txt")
elif sys.argv[1] is 'i':
    xNum = np.loadtxt("xi.txt")
    vNum = np.loadtxt("vi.txt")
    t = np.loadtxt("t.txt")

# Generating analytical data
xAnal = pos(t)
vAnal = vel(t)

# Generating the error between the two
xErr = xAnal - xNum
vErr = vAnal - vNum

# Saving to files
np.savetxt("xErr.txt", xErr)
np.savetxt("vErr.txt", vErr)

# Plotting
fig, (ax1, ax2) = plt.subplots(1, 2)
ax1.set_title("Position Error vs. Time")
ax1.plot(t, xErr)

ax2.set_title("Velocity Error vs. Time")
ax2.plot(t, vErr)

plt.draw()
plt.savefig(filename)

