'''
Plots the phase space x vs. v of given text files.

Usage: phaseSpace.py [option] [filename]

option: 'e' to plot data from explicitEuler.py, 'i' to plot data from 
implicitEuler.py, 's' to plot data from symplecticEuler.py. The text
files these functions produce should be in the same directory as 
phaseSpace.py.

filename: name of the file to save plot to.
'''

import matplotlib.pyplot as plt
import numpy as np
import sys

filename = sys.argv[2]

# Importing data from selected text files.
if sys.argv[1] is 'e':
    x = np.loadtxt('x.txt')
    v = np.loadtxt('v.txt')
    title_str = "v(t) vs. x(t), Explicit Euler"
elif sys.argv[1] is 'i':
    x = np.loadtxt('xi.txt')
    v = np.loadtxt('vi.txt')
    title_str = "v(t) vs. x(t), Implicit Euler"
elif sys.argv[1] is 's':
    x = np.loadtxt('xs.txt')
    v = np.loadtxt('vs.txt')
    title_str = "v(t) vs. x(t), Symplectic Euler"
    
# Plotting the phase space
plt.plot(x, v)

plt.xlabel('x(t)')
plt.ylabel('v(t)')
         
plt.title(title_str)

plt.legend()

plt.savefig(filename)

